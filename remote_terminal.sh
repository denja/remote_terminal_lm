#! /bin/bash

# Get host name
domain=`echo $1 | grep -Po "(?<=host=)[^,]*"`

if [ ${#domain} -eq 0 ]; then
    # Not an SSH directory, just open regular terminal
    gnome-terminal --working-directory="$1"
else
    # Get user name
    user=`echo $1 | grep -Po "(?<=user=)[^/]*"`

    # Get path
    path=`echo $1 | grep -Po "(?<=user="${user}").*"`
    if [ ${#path} -eq 0 ]; then
        path=/
    fi

    # Connect
    gnome-terminal -- ssh ${user}@${domain} -t "cd "${path}" && bash --login"
fi
